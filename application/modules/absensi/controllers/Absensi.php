<?php

class Absensi extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'absensi';
 }

 public function getTableName() {
  return 'absensi_sales';
 }

 public function index() {
  echo $this->getModuleName();
 }

 public function getPostData() {
  $data["pegawai"] = $this->input->post('pegawai');
  $data["date_absen"] = date('Y-m-d');
  $data["status"] = $this->input->post('status');
  $data["lampiran"] = $this->input->post('lampiran');
  $data["createdby"] = $this->input->post('createdby');

  return $data;
 }

 public function submit() {
  $result = array();
  $this->db->trans_begin();
  try {
   $data = $this->getPostData();
   Modules::run('database/_insert', $this->getTableName(), $data);
   $this->db->trans_commit();
   array_push($result, array(
   'is_valid' => '1',
   'message' => "Berhasil Disimpan"
   ));
  } catch (Exception $ex) {
   array_push($result, array(
   'is_valid' => '0',
   'message' => "Gagal Disimpan"
   ));
   $this->db->trans_rollback();
  }

  echo json_encode(array('data' => $result));
 }

}
