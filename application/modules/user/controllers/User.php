<?php

class User extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'user';
 }

 public function getTableName() {
  return 'user';
 }

 public function index() {
  echo $this->getModuleName();
 }

 public function login() {
  $sales_id = $this->input->post('sales_id');
  $password = $this->input->post('password');
  
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName().' u',
  'field' => array('u.id as user', 
  'u.pegawai',
  'p.nik', 
  'p.nama',
  'pr.priveledge as hak_akses'),
  'join' => array(
   array('pegawai p', 'u.pegawai = p.id'),
   array('priveledge pr', 'u.priveledge = pr.id')
  ),
  'where' => array('p.nik' => $sales_id,
  'u.password' => $password)
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->result_array();
   $result[0]['tgl_absen'] = date('D d F Y');
  }
  
  echo json_encode(array('data' => $result));
 }

}
