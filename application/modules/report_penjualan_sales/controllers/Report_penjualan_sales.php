<?php

class Report_penjualan_sales extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'report_penjualan_sales';
 }

 public function getTableName() {
  return 'report_penjualan_sales';
 }

 public function index() {
  echo $this->getModuleName();
 }

 public function getData() {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' rps',  
  'join' => array(
  array('customer_produk cp', 'rps.customer_produk = cp.id')
  )
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  echo json_encode(array('data' => $result));
 }

}
