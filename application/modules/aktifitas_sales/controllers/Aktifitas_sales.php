<?php

class Aktifitas_sales extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'aktifitas_sales';
 }

 public function getTableName() {
  return 'aktifitas';
 }

 public function index() {
  echo $this->getModuleName();
 }

 public function getDataAktifitasSales($aktifitas, $pegawai, $customer) {
  $data = Modules::run('database/get', array(
  'table' => 'sales_aktifitas',
  'where' => array('aktifitas' => $aktifitas,
  'tanggal_aktifitas' => date('Y-m-d'), 
  'pegawai' => $pegawai, 'customer'=> $customer)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getData($pegawai, $customer) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' a',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $data_sales = $this->getDataAktifitasSales($value['id'], $pegawai, $customer);
    if (!empty($data_sales)) {
     $value['sales_aktifitas'] = $data_sales['id'];
     $value['status'] = $data_sales['status'];
    } else {
     $value['sales_aktifitas'] = "";
     $value['status'] = "N";
    }
    $value['customer'] = $customer;
    array_push($result, $value);
   }
  }

  echo json_encode(array('data' => $result));
 }

 public function getPostData() {
  $data["aktifitas"] = $this->input->post('aktifitas');
  $data["pegawai"] = $this->input->post('pegawai');
  $data["foto"] = $this->input->post('foto');
  $data["tanggal_aktifitas"] = date('Y-m-d');
  $data["customer"] = $this->input->post('customer');
  $data["status"] = 'Y';

  return $data;
 }

 public function simpanAktifitas() {
  $result = array();
  $this->db->trans_begin();
  try {
   $data = $this->getPostData();
   Modules::run('database/_insert', 'sales_aktifitas', $data);
   $this->db->trans_commit();
   array_push($result, array(
   'is_valid' => '1',
   'message' => 'Berhasil Disimpan'
   ));
  } catch (Exception $ex) {
   array_push($result, array(
   'is_valid' => '0',
   'message' => 'Gagal Disimpan'
   ));
   $this->db->trans_rollback();
  }

  echo json_encode(array('data' => $result));
 }

 public function submit($pegawai, $customer) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' a',
  ));
  $total_aktifitas = count($data->result_array());

  $detail = Modules::run('database/get', array(
  'table' => 'sales_aktifitas',
  'where' => array('pegawai' => $pegawai,
  'tanggal_aktifitas' => date('Y-m-d'), 'customer'=> $customer)
  ));

  $total_detail = 0;
  if (!empty($detail)) {
   foreach ($detail->result_array() as $value) {
    $total_detail += 1;
   }
  }

  if ($total_detail == $total_aktifitas) {
   echo 'lengkap';
  } else {
   echo 'tidak lengkap';
  }
 }

}
