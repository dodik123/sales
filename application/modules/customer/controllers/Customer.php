<?php

class Customer extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'customer';
 }

 public function getTableName() {
  return 'customer';
 }

 public function index() {
  echo $this->getModuleName();
 }

 public function getData() {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName()
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['last_visit'] = date('d F Y');
    array_push($result, $value);
   }   
  }
  echo json_encode(array('data' => $result));
 }

 public function getDetailCustomer($customer_id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName(),
  'where' => array('id' => $customer_id)
  ));

  $result = array();
  if (!empty($data)) {   
   $result = $data->result_array();
   $result[0]['last_visit'] = date('d F Y');
  }
  echo json_encode(array('data' => $result));
 }
}
