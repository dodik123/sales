<?php

class Stock_entry extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'stock_entry';
 }

 public function getTableName() {
  return 'stock_entry_sales';
 }

 public function index() {
  echo $this->getModuleName();
 }

 public function getDataStockEntry($customer_produk, $pegawai) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName(),
  'where' => array('customer_produk' => $customer_produk,
  'tanggal_entry' => date('Y-m-d'),
  'pegawai' => $pegawai)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getData($pegawai, $customer) {
  $data = Modules::run('database/get', array(
  'table' => 'customer_produk',
  'where' => array('customer' => $customer)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $data_sales = $this->getDataStockEntry($value['id'], $pegawai);
    if (!empty($data_sales)) {
     $value['stock_entry'] = $data_sales['id'];
     $value['stock'] = $data_sales['stock'];
     $value['tanggal_entry'] = $data_sales['tanggal_entry'];
    } else {
     $value['stock_entry'] = "";
     $value['stock'] = "";
     $value['tanggal_entry'] = "";
    }
    $value['customer'] = $customer;
    array_push($result, $value);
   }
  }

  echo json_encode(array('data' => $result));
 }

 public function getPostData() {
  $data['pegawai'] = $this->input->post('pegawai');
  $data['customer_produk'] = $this->input->post('customer_produk');
  $data['tanggal_entry'] = date('Y-m-d');
  $data['stock'] = $this->input->post('stock');

  return $data;
 }

 public function simpanStock() {
  $result = array();
  $this->db->trans_begin();
  try {
   $data = $this->getPostData();
   Modules::run('database/_insert', $this->getTableName(), $data);
   $this->db->trans_commit();
   array_push($result, array(
   'is_valid' => '1',
   'message' => 'Berhasil Disimpan'
   ));
  } catch (Exception $ex) {
   array_push($result, array(
   'is_valid' => '0',
   'message' => 'Gagal Disimpan'
   ));
   $this->db->trans_rollback();
  }

  echo json_encode(array('data' => $result));
 }

 public function submit($pegawai, $customer) {
  $data = Modules::run('database/get', array(
  'table' => 'customer_produk',
  'where' => array('customer' => $customer)
  ));

  $total_produk = count($data->result_array());

  $total_detail = 0;
  foreach ($data->result_array() as $value) {
   $detail = Modules::run('database/get', array(
   'table' => $this->getTableName(),
   'where' => array('pegawai' => $pegawai,
   'tanggal_entry' => date('Y-m-d'),
   'customer_produk' => $value['id'])
   ));
   if (!empty($detail)) {
    $total_detail += 1;
   }
  }


  if ($total_detail == $total_produk) {
   echo 'lengkap';
  } else {
   echo 'tidak lengkap';
  }
 }

}
